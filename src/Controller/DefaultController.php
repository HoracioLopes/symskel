<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController {
  /**
   * @Route("/", name="index")
   */
  public function index() {
    return $this->render('base.html.twig', []);
  }

  /**
   * @Route("/response", name="response")
   */
  public function response() {
    $response = new Response();
    $response->setContent("Some random text");
    $response->headers->set('Content-Type', 'text/plain');
    $response->headers->set('Access-Control-Allow-Origin', '*');
    $response->setStatusCode(200);
    return $response;
  }
}
